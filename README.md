# Portfolio performance

Using this application user can enter the state of his/her assets portfolio at some day in the past and see how much money it would be worth today.

Unfortunately I spent almost all my time on building core logic, api client for worldtradingdata.com and tests so web interface is dead simple HTML without almost any CSS. It's deployed to Heroku and you can access it here: https://portfolio-perf.herokuapp.com/

The form is prefilled with example data so you can press calculate to calculate results right away. Right now it's hardcoded to only 2 types of stocks since that's the amount worldtradingdata.com supports with Free license (but the backend code supports any number of stocks).

Since I had time restrictions I don't really validate any data that comes from the client but in real life application it's a must for sure. Also all the data conversions happens in the router itself and it's not optiomal and I'd extract it to some modules as well.

This application is just Elixr app with Plug library to expose it to web. I intentionally haven't used Phoenix just to see how is it to implement basic web app without any advanced framework. Because of that it doesn't have any controller which we used to see in classic Phoenix/Rails apps and everything happens in router.ex.

API client for worldtradingdata.com was written the way it can be easily mocked in integration tests. As you can see the client module is specified through `Application.get_env(:portfolio_performance, :world_trading_data_client)` which is replced with mock module in test env.

Also all the calculations are made with float number and ideally it should be done with some sort of `money` library as https://hex.pm/packages/ex_money to have precise results.

To run app locally just call `MIX_ENV=prod mix run --no-halt`.
