use Mix.Config

config :portfolio_performance, :world_trading_data_client, PortfolioPerformance.WorldTradingData.Client
config :portfolio_performance, :world_trading_data_api_token, System.get_env("WORLD_TRADING_API_TOKEN")
