defmodule PortfolioPerformance do
  alias PortfolioPerformance.Portfolio

  def initial_portfolio(%{:balance => balance, :date => date, :stocks => stocks} = params, historical_data) do
    stocks_amounts = Enum.map stocks, fn {stock, allocation} ->
      stock_price = historical_data[stock]

      %{
        name: stock,
        amount: (allocation * balance) / stock_price,
        price: stock_price,
        worth: allocation * balance
      }
    end

    %Portfolio{
      balance: balance,
      date: date,
      stocks: stocks_amounts
    }
  end

  def current_portfolio(initial_portfolio: initial_portfolio, date: date, historical_data: historical_data) do
    stocks_price = Enum.map initial_portfolio.stocks, fn %{name: name, amount: amount} ->
      stock_price = historical_data[name]

      %{
        name: name,
        amount: amount,
        price: stock_price,
        worth: amount * stock_price
      }
    end

    balance = Enum.reduce(stocks_price, 0, fn %{worth: worth}, acc -> acc + worth end)

    %Portfolio{
      balance: balance,
      date: date,
      stocks: stocks_price
    }
  end
end
