defmodule PortfolioPerformanceWeb.Application do
  use Application
  require Logger

  def start(_type, _args) do
    children = [
      {Plug.Cowboy, scheme: :http, plug: PortfolioPerformanceWeb.Router, options: [port: port()]}
    ]

    opts = [strategy: :one_for_one, name: PortfolioPerformance.Supervisor]

    Supervisor.start_link(children, opts)
  end

  defp port do
    System.get_env()
    |> Map.get("PORT", "8080")
    |> String.to_integer()
  end
end
