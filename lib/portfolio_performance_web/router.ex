defmodule PortfolioPerformanceWeb.Router do
  use Plug.Router

  plug Plug.Parsers, parsers: [:urlencoded, :multipart]

  plug :match
  plug :dispatch

  alias PortfolioPerformance.WorldTradingData.HistoricalData

  @template_dir "lib/portfolio_performance_web/templates"

  get "/" do
    render(conn, "index.html")
  end

  post "/" do
    start_date = conn.params["start_date"]
    current_date = conn.params["current_date"]
    {balance, _} = Float.parse(conn.params["balance"])
    stock_names = conn.params["stock_name"]

    stock_shares = Enum.map(
      conn.params["stock_share"], fn stock_share ->
        {parsed, _} = Float.parse(stock_share)

        parsed
    end)

    stocks = Enum.zip(stock_names, stock_shares) |> Map.new

    {:ok, current_data} = HistoricalData.stocks_historical_data(
      %{date: current_date, stocks: stock_names}
    )

    {:ok, start_data} = HistoricalData.stocks_historical_data(
      %{date: start_date, stocks: stock_names}
    )

    initial_portfolio = PortfolioPerformance.initial_portfolio(
      %{:balance => balance, :date => start_date, :stocks => stocks},
      start_data
    )

    current_portfolio = PortfolioPerformance.current_portfolio(
      initial_portfolio: initial_portfolio,
      date: current_date,
      historical_data: current_data
    )

    render(conn, "results.html", current_portfolio: current_portfolio)
  end

  defp render(%{status: status} = conn, template, assigns \\ []) do
    body =
      @template_dir
      |> Path.join(template)
      |> String.replace_suffix(".html", ".html.eex")
      |> EEx.eval_file(assigns)

    send_resp(conn, (status || 200), body)
  end
end
