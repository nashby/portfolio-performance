defmodule PortfolioPerformance.WorldTradingData.HistoricalData do
  @world_trading_data_client Application.get_env(:portfolio_performance, :world_trading_data_client)

  def stocks_historical_data(%{date: date, stocks: stocks}) do
    request_params = %{
      date: date,
      symbol: Enum.join(stocks, ",")
    }

    case @world_trading_data_client.make_request(:get, "/history_multi_single_day", request_params) do
      {:ok, data} ->
        historical_data = Enum.map(stocks, fn stock ->
          {price, _} = Float.parse(data["data"][stock]["close"])

          {stock, price}
        end) |> Map.new

        {:ok, historical_data}

      {:error, message} -> {:error, message}
    end
  end
end

