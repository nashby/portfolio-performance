defmodule PortfolioPerformance.WorldTradingData.Client do
  use HTTPoison.Base

  @world_trading_url "https://api.worldtradingdata.com/api/v1"

  def make_request(:get, url, params \\ %{}) do
    params = Map.put(params, :api_token, api_token())

    case get!(url, [], params: params) do
      %HTTPoison.Response{status_code: 200, body: body} ->
        {:ok, body}
      %HTTPoison.Response{status_code: code} ->
        {:error, "Responded with #{code}"}
    end
  end

  defp process_url(path), do: @world_trading_url <> path

  defp process_response_body(body) do
    body |> Jason.decode!
  end

  defp api_token do
    Application.get_env(:portfolio_performance, :world_trading_data_api_token)
  end
end
