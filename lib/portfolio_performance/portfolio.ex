defmodule PortfolioPerformance.Portfolio do
  defstruct balance: 0, date: nil, stocks: []
end
