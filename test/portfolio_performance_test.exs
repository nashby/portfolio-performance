defmodule PortfolioPerformanceTest do
  use ExUnit.Case
  doctest PortfolioPerformance

  alias PortfolioPerformance.Portfolio

  test "builds initial portfolio" do
    historical_data = historical_data_2018_01_02()

    portfolio = PortfolioPerformance.initial_portfolio(
      %{:balance => 32500, :date => "2018-01-02", :stocks => %{"AAPL" => 0.4, "GOOG" => 0.6}},
      historical_data
    )

    expected_portfolio = %Portfolio{
      :balance => 32500,
      :date => "2018-01-02",
      :stocks => [
        %{
          name: "AAPL",
          amount: 75.4673168466272,
          price: 172.26,
          worth: 13000
        },

        %{
          name: "GOOG",
          amount: 18.309859154929576,
          price: 1065.0,
          worth: 19500
        },
      ]
    }

    assert portfolio == expected_portfolio
  end

  test "builds current portfolio" do
    historical_data = historical_data_2019_06_20()

    initial_portfolio = %Portfolio{
      :balance => 32500,
      :date => "2018-01-02",
      :stocks => [
        %{
          name: "AAPL",
          amount: 75.4673168466272,
          price: 172.26,
          worth: 13000
        },

        %{
          name: "GOOG",
          amount: 18.309859154929576,
          price: 1065.0,
          worth: 19500
        },
      ]
    }

    current_portfolio = PortfolioPerformance.current_portfolio(
      initial_portfolio: initial_portfolio, date: "2019-06-20", historical_data: historical_data
    )

    expected_portfolio = %Portfolio{
      :balance => 35402.654680200096,
      :date => "2019-06-20",
      :stocks => [
        %{
          name: "AAPL",
          amount: 75.4673168466272,
          price: 199.46,
          worth: 15052.711018228261
        },

        %{
          name: "GOOG",
          amount: 18.309859154929576,
          price: 1111.42,
          worth: 20349.943661971833
        },
      ]
    }

    assert current_portfolio == expected_portfolio
  end

  def historical_data_2018_01_02() do
    %{
      "AAPL" => 172.26,
      "GOOG" => 1065.00
    }
  end

  def historical_data_2019_06_20() do
    %{
      "AAPL" => 199.46,
      "GOOG" => 1111.42
    }
  end
end
