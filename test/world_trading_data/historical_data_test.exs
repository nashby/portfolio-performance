defmodule PortfolioPerformance.Test.WorldTradingData.HistoricalDataTest do
  use ExUnit.Case

  alias PortfolioPerformance.WorldTradingData.HistoricalData

  test "returns historical data" do
    historical_data = HistoricalData.stocks_historical_data(
      %{date: "2013-03-20", stocks: ["AAPL", "GOOG"]}
    )

    assert historical_data == {:ok, %{"AAPL" => 64.58, "GOOG" => 407.35}}
  end
end
