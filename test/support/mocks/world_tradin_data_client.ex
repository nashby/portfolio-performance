defmodule PortfolioPerformance.Test.WorldTradingData.Client do
  def make_request(:get, "/history_multi_single_day", %{date: "2013-03-20", symbol: "AAPL,GOOG"}) do
    {
      :ok,
      %{
        "date" => "2013-03-20",
        "data" => %{
          "AAPL" => %{
            "close" => "64.58",
            "high" => "65.38",
            "low" => "64.23",
            "open" => "65.36",
            "volume" => "11023594"
          },
          "GOOG" => %{
            "close" => "407.35",
            "high" => "408.75",
            "low" => "405.72",
            "open" => "408.74",
            "volume" => "1464122"
          }
        }
      }
    }
  end
end
